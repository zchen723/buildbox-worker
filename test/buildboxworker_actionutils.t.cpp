#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>
#include <gmock/gmock.h>
#include <google/devtools/remoteworkers/v1test2/bots.grpc.pb.h>
#include <gtest/gtest.h>

#include <buildboxworker_actionutils.h>
#include <buildboxworker_metricnames.h>
#include <buildboxworker_worker.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_temporaryfile.h>
#include <buildboxcommonmetrics_durationmetricvalue.h>
#include <buildboxcommonmetrics_testingutils.h>

namespace proto {
using namespace google::devtools::remoteworkers::v1test2;
using namespace build::bazel::remote::execution::v2;
} // namespace proto

using namespace testing;
using buildboxcommon::TemporaryFile;
using buildboxcommon::buildboxcommonmetrics::collectedByName;
using buildboxcommon::buildboxcommonmetrics::DurationMetricValue;
using buildboxworker::ActionUtils;

TEST(ActionUtilsTests, ReadActionResultFileMissing)
{
    const std::string actionFileName("this_file_does_not_exist.txt");
    ASSERT_FALSE(
        buildboxcommon::FileUtils::isRegularFile(actionFileName.c_str()));
    ASSERT_THROW(ActionUtils::readActionResultFile(actionFileName.c_str()),
                 std::runtime_error);
}

TEST(ActionUtilsTests, ReadActionResultFileInvalidFile)
{
    const std::string actionFileName("invalid_actionresult.txt");
    ASSERT_TRUE(
        buildboxcommon::FileUtils::isRegularFile(actionFileName.c_str()));
    EXPECT_THROW(ActionUtils::readActionResultFile(actionFileName.c_str()),
                 std::runtime_error);
}

TEST(ActionUtilsTests, ReadActionResultFileEmptyFile)
{
    buildboxcommon::TemporaryFile emptyActionResult;
    EXPECT_THROW(ActionUtils::readActionResultFile(emptyActionResult.name()),
                 std::runtime_error);
}

TEST(ActionUtilsTests, ReadActionResultFileNormal)
{
    const std::string actionFileName("sample_actionresult.txt");
    auto result = ActionUtils::readActionResultFile(actionFileName.c_str());
    ASSERT_EQ(result.exit_code(), 0);
}

TEST(ActionUtilsTests, ReadActionResultFileMetricsCollected)
{
    const std::string actionFileName("sample_actionresult.txt");
    ActionUtils::readActionResultFile(actionFileName.c_str());
    ASSERT_TRUE(collectedByName<DurationMetricValue>(
        buildboxworker::MetricNames::TIMER_NAME_READ_ACTION_RESULT));
}

class MockActionUtils : public ActionUtils {
    MOCK_METHOD1(runCommandInSubprocess,
                 pid_t(const std::vector<std::string> args));
    MOCK_METHOD2(waitForRunner,
                 void(pid_t subprocessPid, buildboxworker::Worker *worker));
};

TEST(ActionUtilsTests, ExecuteActionInSubprocessMetricsCollected)
{
    buildboxworker::Worker worker;
    MockActionUtils::executeActionInSubprocess({"ls"}, &worker);
    ASSERT_TRUE(collectedByName<DurationMetricValue>(
        buildboxworker::MetricNames::TIMER_NAME_EXECUTE_ACTION));
}

TEST(ActionUtilsTests, ReadStatusFromFile)
{
    google::rpc::Status status;
    status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
    status.set_message("Something wasn't as expected");

    buildboxcommon::TemporaryFile file;
    ASSERT_NO_THROW(
        buildboxcommon::ProtoUtils::writeProtobufToFile(status, file.name()));

    google::rpc::Status readStatus;
    ASSERT_TRUE(ActionUtils::readStatusFile(file.name(), &readStatus));

    ASSERT_EQ(readStatus.message(), status.message());
    ASSERT_EQ(readStatus.code(), status.code());
}

TEST(ActionUtilsTests, ReadStatusFails)
{

    const auto path = "/the/file/does/not/exist";
    ASSERT_FALSE(buildboxcommon::FileUtils::isRegularFile(path));

    google::rpc::Status status;
    EXPECT_FALSE(ActionUtils::readStatusFile(path, &status));

    ASSERT_EQ(status.message(), google::rpc::Status().message());
    ASSERT_EQ(status.code(), google::rpc::Status().code());
}

class DownloadActionTests : public ::testing::Test {
  protected:
    TemporaryFile testActionFile;
    proto::BotSession botSession;
    // std::string casServer = "https://foobar";
    buildboxcommon::ConnectionOptions casServerConnection;
    proto::Lease *lease;
    DownloadActionTests()
    {
        lease = botSession.add_leases();
        lease->set_id("42");
    }
};

TEST_F(DownloadActionTests, DownloadActionMetricsCollected)
{
    // Action:
    proto::Action action;
    action.mutable_timeout()->set_seconds(123);
    action.mutable_command_digest()->set_hash("command-hash");
    action.mutable_command_digest()->set_size_bytes(32);

    const buildboxcommon::Digest actionDigest =
        buildboxcommon::CASHash::hash(action.SerializeAsString());

    // Lease containing the Action:
    proto::Lease lease;
    lease.set_id("lease-id");
    lease.mutable_payload()->PackFrom(action);

    proto::Action returnedAction;
    proto::Digest returnedActionDigest;
    std::tie(returnedAction, returnedActionDigest) =
        ActionUtils::getActionFromLease(lease,
                                        buildboxcommon::ConnectionOptions());

    ASSERT_EQ(returnedActionDigest, buildboxcommon::CASHash::hash(
                                        returnedAction.SerializeAsString()));
    ASSERT_EQ(returnedActionDigest, actionDigest);

    ASSERT_TRUE(collectedByName<DurationMetricValue>(
        buildboxworker::MetricNames::TIMER_NAME_DOWNLOAD_ACTION));
}
