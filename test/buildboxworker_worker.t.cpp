#include <gtest/gtest.h>

#include <google/devtools/remoteworkers/v1test2/bots_mock.grpc.pb.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_grpctestserver.h>
#include <buildboxcommon_protos.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_testingutils.h>
#include <buildboxworker_expiretime.h>
#include <buildboxworker_metricnames.h>
#include <buildboxworker_runnercommandutils.h>
#include <buildboxworker_worker.h>

#include <algorithm>

namespace proto {
using namespace google::bytestream;
using namespace google::devtools::remoteworkers::v1test2;
using namespace build::bazel::remote::execution::v2;
} // namespace proto

using buildboxcommon::buildboxcommonmetrics::DurationMetricValue;
using buildboxworker::Worker;
using google::devtools::remoteworkers::v1test2::MockBotsStub;

using namespace testing;

class WorkerTests : public ::testing::Test {
  protected:
    buildboxworker::Worker worker;
    WorkerTests() { worker.d_stub = std::make_unique<MockBotsStub>(); }
};

class WorkerTestFixture : public Worker, public ::testing::Test {
  public:
    WorkerTestFixture()
    {
        d_logLevel = "debug";
        // Set path to test runner
        d_runnerCommand = getenv("BUILDBOX_RUN");
    }
    void addFakeJob(const std::string jobString)
    {
        d_activeJobs.insert(jobString);
    }
    void runSingleLease(const proto::Lease &lease,
                        proto::Lease *completedLease)
    {
        ASSERT_EQ(lease.state(), proto::LeaseState::PENDING);
        d_maxConcurrentJobs = 1;
        d_stopAfterJobs = 1;
        d_session.add_leases()->CopyFrom(lease);
        runWorkerWithoutBotSession();
        completedLease->CopyFrom(d_session.leases(0));
        EXPECT_EQ(completedLease->state(), proto::LeaseState::COMPLETED);
    }
    void setBotStatus(const proto::BotStatus newStatus)
    {
        d_botStatus = newStatus;
    }
};

// Test that even if a long deadline is set with no work given
// the worker returned a short wait time
TEST_F(WorkerTestFixture, WaitTimeNoJobs)
{
    const auto currentTime = std::chrono::system_clock::now();
    const auto excessive_wait_time =
        std::chrono::duration_cast<std::chrono::seconds>(2 *
                                                         this->s_maxWaitTime);

    const auto expectedWaitTime = currentTime + this->s_defaultWaitTime;

    this->d_session.mutable_expire_time()->set_seconds(
        std::chrono::system_clock::to_time_t(currentTime) +
        excessive_wait_time.count());

    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedWaitTime);
}

// If the bot is in a non-ok status, wait time should be respected
// even if the bot has no work.
TEST_F(WorkerTestFixture, WaitTimeNoJobsUnhealthyBot)
{
    // Very specifically calculate the expire time
    // to avoid rounding errors
    const auto currentTime =
        std::chrono::time_point_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now());
    auto deadline = currentTime + this->s_maxWaitTime;
    const auto deadlineSeconds =
        std::chrono::duration_cast<std::chrono::seconds>(
            deadline.time_since_epoch());
    deadline -= deadlineSeconds;
    const auto deadlineRemainder =
        std::chrono::duration_cast<std::chrono::microseconds>(
            deadline.time_since_epoch());

    // Set the expire time in the seconds/nanos format protobuf expects
    this->d_session.mutable_expire_time()->set_seconds(
        deadlineSeconds.count());
    this->d_session.mutable_expire_time()->set_nanos(
        deadlineRemainder.count() * 1e3);

    // Calculate the expected reduction in duration
    const auto factor = static_cast<int64_t>(
        this->s_maxWaitTime.count() *
        buildboxworker::ExpireTime::updateTimeoutPaddingFactor());

    const auto expectedExpireTime =
        currentTime +
        (this->s_maxWaitTime - std::chrono::microseconds(factor));

    // Set the bot as Unhealthy to make it respect the given expire_time
    // even when there's no assigned work
    setBotStatus(proto::BotStatus::UNHEALTHY);
    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedExpireTime);
}

TEST_F(WorkerTestFixture, DefaultWaitTime)
{
    this->addFakeJob("testjob");
    const auto currentTime = std::chrono::system_clock::now();
    const auto expectedWaitTime = currentTime + this->s_defaultWaitTime;

    ASSERT_FALSE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedWaitTime);
}

TEST_F(WorkerTestFixture, WaitTimeSaturates)
{
    this->addFakeJob("testjob");
    const auto currentTime = std::chrono::system_clock::now();
    const auto excessive_wait_time =
        std::chrono::duration_cast<std::chrono::seconds>(2 *
                                                         this->s_maxWaitTime);
    const auto expectedWaitTime = currentTime + this->s_maxWaitTime;

    this->d_session.mutable_expire_time()->set_seconds(
        std::chrono::system_clock::to_time_t(currentTime) +
        excessive_wait_time.count());

    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedWaitTime);
}

TEST_F(WorkerTestFixture, WaitTimePercentage)
{
    this->addFakeJob("testjob");
    // Use a starting time with microsecond precision to prevent rounding
    // errors in test as we use `microseconds` throughout
    const auto currentTime =
        std::chrono::time_point_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now());

    // Calculate the deadline in the protobuf format (seconds / nanosecond
    // remainder)
    auto deadline = currentTime + this->s_maxWaitTime;
    const auto deadlineSeconds =
        std::chrono::duration_cast<std::chrono::seconds>(
            deadline.time_since_epoch());
    deadline -= deadlineSeconds;
    const auto deadlineRemainder =
        std::chrono::duration_cast<std::chrono::microseconds>(
            deadline.time_since_epoch());

    this->d_session.mutable_expire_time()->set_seconds(
        deadlineSeconds.count());
    this->d_session.mutable_expire_time()->set_nanos(
        deadlineRemainder.count() * 1e3);

    // Calculate the expected reduction in duration
    const auto factor = static_cast<int64_t>(
        this->s_maxWaitTime.count() *
        buildboxworker::ExpireTime::updateTimeoutPaddingFactor());

    const auto expectedExpireTime =
        currentTime +
        (this->s_maxWaitTime - std::chrono::microseconds(factor));

    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedExpireTime);
}

TEST_F(WorkerTestFixture, RunnerCommand)
{
    d_runnerCommand = "/bin/buildbox-run";
    // Specifying a full path because otherwise `Worker::buildRunnerCommand()`
    // will attempt to look up the command and throw due to it not existing,
    // while a path will remain unmodified.

    buildboxcommon::TemporaryFile actionFile;
    buildboxcommon::TemporaryFile actionResultFile;
    buildboxcommon::TemporaryFile stdoutFile;
    buildboxcommon::TemporaryFile stderrFile;

    const std::vector<std::string> runnerCommand = buildRunnerCommand(
        actionFile, actionResultFile, stdoutFile, stderrFile);

    std::vector<std::string> expectedCommand =
        buildboxworker::RunnerCommandUtils::buildRunnerCommand(
            d_runnerCommand, d_extraRunArgs, actionFile.strname(),
            actionResultFile.strname(), d_casServer, d_logLevel,
            d_logDirectory);
    buildboxworker::RunnerCommandUtils::setRunnerStderrStdoutFile(
        stdoutFile.strname(), stderrFile.strname(), &expectedCommand);

    ASSERT_TRUE(std::equal(runnerCommand.cbegin(), runnerCommand.cend(),
                           expectedCommand.cbegin(), expectedCommand.cend()));
}

proto::Command createSleepCommand(int time)
{
    proto::Command command;
    command.add_arguments("/usr/bin/env");
    command.add_arguments("sleep");
    command.add_arguments(std::to_string(time));
    return command;
}

google::rpc::Status runAction(WorkerTestFixture *worker, proto::Action *action,
                              const proto::Command &command,
                              proto::ActionResult *actionResult)
{
    const auto commandSerialized = command.SerializeAsString();
    const auto commandDigest =
        buildboxcommon::CASHash::hash(commandSerialized);
    action->mutable_command_digest()->CopyFrom(commandDigest);

    buildboxcommon::GrpcTestServer testServer;
    std::thread serverHandler([&]() {
        buildboxcommon::GrpcTestServerContext ctxCap(
            &testServer,
            "/build.bazel.remote.execution.v2.Capabilities/GetCapabilities");
        ctxCap.finish(grpc::Status(grpc::UNIMPLEMENTED, "Unimplemented"));

        proto::ReadRequest expectedReadRequest;
        proto::ReadResponse readResponse;
        expectedReadRequest.set_resource_name(
            "blobs/" + commandDigest.hash() + "/" +
            std::to_string(commandDigest.size_bytes()));
        readResponse.set_data(commandSerialized);
        buildboxcommon::GrpcTestServerContext ctx(
            &testServer, "/google.bytestream.ByteStream/Read");
        ctx.read(expectedReadRequest);
        ctx.writeAndFinish(readResponse);
    });

    worker->d_casServer.setUrl(testServer.url());
    worker->d_extraRunArgs.push_back("--no-logs-capture");

    proto::Lease completedLease;
    try {
        proto::Lease lease;
        lease.set_id("test-lease");
        lease.mutable_payload()->PackFrom(*action);
        lease.set_state(proto::LeaseState::PENDING);

        worker->runSingleLease(lease, &completedLease);

        EXPECT_TRUE(completedLease.result().UnpackTo(actionResult));
    }
    catch (...) {
        serverHandler.join();
        throw;
    }
    serverHandler.join();
    return completedLease.status();
}

TEST_F(WorkerTestFixture, RunSingleLease)
{
    proto::Action action;
    proto::Command command = createSleepCommand(1);

    proto::ActionResult actionResult;
    const auto status = runAction(this, &action, command, &actionResult);

    EXPECT_EQ(status.code(), grpc::StatusCode::OK);
    EXPECT_EQ(actionResult.exit_code(), 0);
}

TEST_F(WorkerTestFixture, RunSingleLeaseTimeout)
{
    proto::Action action;
    action.mutable_timeout()->CopyFrom(
        google::protobuf::util::TimeUtil::SecondsToDuration(1));
    proto::Command command = createSleepCommand(5);

    proto::ActionResult actionResult;
    const auto status = runAction(this, &action, command, &actionResult);

    EXPECT_EQ(status.code(), grpc::StatusCode::DEADLINE_EXCEEDED);
    EXPECT_EQ(actionResult.exit_code(), 128 + SIGKILL);
}
