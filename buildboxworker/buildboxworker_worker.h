/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_WORKER
#define INCLUDED_BUILDBOXWORKER_WORKER

#include <buildboxcommon_commandline.h>
#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_protos.h>
#include <buildboxcommon_standardoutputstreamer.h>
#include <buildboxcommon_temporaryfile.h>
#include <buildboxcommonmetrics_metricsconfigtype.h>

#include <google/devtools/remoteworkers/v1test2/bots.grpc.pb.h>

#include <condition_variable>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <vector>

namespace buildboxworker {

namespace proto {
using namespace google::devtools::remoteworkers::v1test2;
} // namespace proto

class SubprocessGuard;

class Worker {
    friend class SubprocessGuard;

  public:
    Worker() {}
    Worker(const buildboxcommon::CommandLine &cml, const std::string &botId,
           buildboxcommon::buildboxcommonmetrics::MetricsConfigType
               *metricsConfig);

    bool validateConfiguration();

    /**
     * Connect to the Bots and CAS servers and run jobs until
     * `d_stopAfterJobs` reaches 0.
     */
    void runWorker();

    /**
     * Allows verifying that a runner CLI specification is correct. I.e., that
     * the path to the binary is valid, that it supports all the given options,
     * and that we are setting all the required ones.
     *
     * For that it generates a command with `buildRunnerCommand()` and appends
     * the `--validate-parameters` option to it.
     *
     * Returns whether the call succeeded.
     */
    bool testRunnerCommand();

    buildboxcommon::ConnectionOptions d_botsServer;
    buildboxcommon::ConnectionOptions d_casServer;
    buildboxcommon::ConnectionOptions d_logStreamServer;

    std::shared_ptr<proto::Bots::StubInterface> d_stub;
    std::string d_botID;
    std::string d_instanceName;
    std::vector<std::pair<std::string, std::string>> d_platform;
    std::vector<std::string> d_extraRunArgs;

    int d_maxConcurrentJobs;

    // The worker will stop running after executing this many jobs. If this
    // is negative (the default), it will never stop.
    int d_stopAfterJobs;

    std::string d_runnerCommand = "buildbox-run";

    std::string d_logLevel;
    std::string d_logDirectory;
    std::string d_configFileName;

    // The group pids of all spawned subprocesses (set to their pids)
    std::set<pid_t> d_subprocessPgid;
    proto::BotStatus d_botStatus = proto::BotStatus::OK;

    // Decrements the thread counter and issues a notification.
    void decrementThreadCount();

  protected: // (Allow access to test fixtures)
    // Default time between `UpdateBotSession()` calls:
    static const std::chrono::microseconds s_defaultWaitTime;
    // Max. time between `UpdateBotSession()` calls:
    static const std::chrono::microseconds s_maxWaitTime;

    proto::BotSession d_session;

    // Keep track of Lease IDs that have been assigned to a worker
    // Lease IDs are added when the while-loop spawns worker threads and
    // are removed when by the worker threads when they're done.
    std::set<std::string> d_activeJobs;

    // Stores the `ExecuteOperationMetadata` messages that were sent as
    // metadata in the last `UpdateBotSession()` call (it is overwritten
    // periodically). This should be accessed under the session mutex.
    // The entries are indexed by `ExecuteOperationMetadata.action_digest()`.
    std::unordered_multimap<buildboxcommon::Digest,
                            buildboxcommon::ExecuteOperationMetadata>
        d_sessionExecuteOperationMetadata;
    // In order for the worker to enable the streaming of standard outputs, the
    // execution server can specify an endpoint by attaching an
    // `ExecuteOperationMetadata` message `m` under name
    // "executeoperationmetadata-bin" with:
    //
    //  * `m.action_digest()` equal to the digest of the pertinent action in
    //  the `Lease`, and
    //
    //  * `m.stdout_stream_name()` pointing to a ByteStream resource name.

    // Returns the time to wait for a job. (That time is based on the value of
    // `BotSession::expire_time` and the availability of active jobs.)
    std::chrono::system_clock::time_point
    calculateWaitTime(const std::chrono::system_clock::time_point currentTime =
                          std::chrono::system_clock::now()) const;

    // Build the command line to invoke the runner
    std::vector<std::string>
    buildRunnerCommand(buildboxcommon::TemporaryFile &actionFile,
                       buildboxcommon::TemporaryFile &actionResultFile,
                       buildboxcommon::TemporaryFile &stdoutFile,
                       buildboxcommon::TemporaryFile &stderrFile);

    // Run worker without bot session for testing purposes
    void runWorkerWithoutBotSession();

  private:
    /**
     * Execute the action stored in the given temporary file.
     */

    google::rpc::Status
    executeAction(buildboxcommon::TemporaryFile &actionFile,
                  const std::string &stdoutStream,
                  const std::string &stderrStream,
                  buildboxcommon::TemporaryFile &actionResultFile,
                  buildboxcommon::ActionResult *actionResult);

    void workerThread(const std::string &leaseId);

    // Searches for the Action corresponding to the given lease.
    // If found, exports the Action to the file, writes its digest and
    // returns true.
    // Otherwise, returns false or throws `std::runtime_error` on I/O errors
    // writing the action to the file.
    // If the lease is not found it will also decrement the thread count and
    // wake up other threads.
    bool fetchAction(const std::string &leaseId,
                     buildboxcommon::TemporaryFile &actionFile,
                     buildboxcommon::Digest *actionDigest);

    // Update the lease with the status of its execution. If `status.ok()`,
    // ActionResult is added to the lease.
    // It also decrements the thread count and removes the lease from the
    // active jobs list.
    void
    storeActionResultInLease(const std::string &leaseId,
                             const google::rpc::Status &leaseStatus,
                             const buildboxcommon::ActionResult &actionResult);

    std::mutex d_sessionMutex;

    // Notified when a worker thread finishes a job.
    std::condition_variable d_sessionCondition;

    // Total number of detached threads.
    int d_detachedThreadCount = 0;

    // Keep track of Lease IDs that have been accepted by the worker, but
    // whose acceptance has not yet been acknowledged by the server (so we
    // haven't actually started work on them yet).
    std::set<std::string> d_jobsPendingAck;

    // Register the signal handler. On errors `exit(1)`.
    static void registerSignals();

    // Set the platform properties for `d_session`.
    void setPlatformProperties();

    // Returns whether the main loop in `runWorker()` should keep going.
    bool hasJobsToProcess() const;

    // Functions to process the session's leases.
    void processLeases(bool *skipPollDelay);
    void processPendingLease(proto::Lease *lease, bool *skipPollDelay);
    void processActiveLease(const proto::Lease &lease);
    void processCancelledLease(const proto::Lease &lease);

    // If connected to a LogStream server when resource names were specified,
    // start streaming the contents of the standard outputs. If not, this
    // function has no effect.
    // (When `LOGSTREAM_DEBUG` is defined, launches `d_logstreamDebugCommand`.)
    void setUpStreamingIfNeeded(
        const std::string &stdoutStream, const std::string &stdoutFilePath,
        const std::string &stderrStream, const std::string &stderrFilePath,
        std::unique_ptr<buildboxcommon::StandardOutputStreamer>
            *stdoutStreamer,
        std::unique_ptr<buildboxcommon::StandardOutputStreamer>
            *stderrStreamer);

    // Call `stop()` and log the result for pointers to running instances.
    // For nullptrs it will have no effect.
    void stopStreamingIfNeeded(
        const std::unique_ptr<buildboxcommon::StandardOutputStreamer>
            &stdoutStreamer,
        const std::unique_ptr<buildboxcommon::StandardOutputStreamer>
            &stderrStreamer);

    // Send a SIGTERM to all tracked subprocesses groups
    void shutdownTrackedSubprocessPgids();

    void logSuppliedParameters() const;

    // Add a subprocess to this worker's tracked subprocesses
    void trackSubprocess(const pid_t subprocessPid);

    // Remove a subprocess from this worker's tracked subprocesses
    void untrackSubprocess(const pid_t subprocessPid);

    // Given an action digest, searches
    // `d_sessionExecuteOperationMetadata` for an entry that refers to that
    // action. If one is found, returns a pair (`stdout_stream_name`,
    // `stderr_stream_name`) otherwise returns an empty string.
    std::pair<std::string, std::string> standardOutputsStreamNames(
        const buildboxcommon::Digest &actionDigest) const;

#ifdef LOGSTREAM_DEBUG
    std::string d_logstreamDebugCommand;
#endif
};

} // namespace buildboxworker
#endif
