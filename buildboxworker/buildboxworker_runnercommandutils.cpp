/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_runnercommandutils.h>

#include <buildboxcommon_exception.h>

namespace buildboxworker {

std::vector<std::string> RunnerCommandUtils::buildRunnerCommand(
    const std::string &runnerBinaryPath,
    const std::vector<std::string> &runnerExtraArguments,
    const std::string &actionFilePath, const std::string &actionResultFilePath,
    const buildboxcommon::ConnectionOptions &casConnectionOptions,
    const std::string &logLevel, const std::string &logDirectory)
{
    std::vector<std::string> command = {runnerBinaryPath};

    command.insert(command.end(), runnerExtraArguments.cbegin(),
                   runnerExtraArguments.cend());
    casConnectionOptions.putArgs(&command);

    command.emplace_back("--action=" + actionFilePath);
    command.emplace_back("--action-result=" + actionResultFilePath);

    command.emplace_back("--log-level=" + logLevel);

    if (!logDirectory.empty()) {
        command.emplace_back("--log-directory=" + logDirectory);
    }

    return command;
}

void RunnerCommandUtils::setRunnerStderrStdoutFile(
    const std::string &stdoutFilePath, const std::string &stderrFilePath,
    std::vector<std::string> *command)
{
    if (command == nullptr) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Pointer to command vector is nullptr");
    }

    command->emplace_back("--stdout-file=" + stdoutFilePath);
    command->emplace_back("--stderr-file=" + stderrFilePath);
}

} // namespace buildboxworker
