/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_SUBPROCESSGUARD
#define INCLUDED_BUILDBOXWORKER_SUBPROCESSGUARD

#include <sys/types.h>

namespace buildboxworker {

class Worker;

class SubprocessGuard {
  public:
    SubprocessGuard(const pid_t &subprocessPid, Worker *worker);
    ~SubprocessGuard();

  private:
    Worker *d_worker;
    pid_t d_subprocessPid;
};

} // namespace buildboxworker

#endif
