/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_botsessionutils.h>
#include <buildboxworker_metricnames.h>

#include <buildboxcommon_grpcclient.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>

namespace buildboxworker {

using namespace buildboxcommon;

grpc::Status BotSessionUtils::updateBotSession(
    const proto::BotStatus botStatus,
    std::shared_ptr<proto::Bots::StubInterface> stub,
    const buildboxcommon::ConnectionOptions &botsServerConnection,
    proto::BotSession *session,
    const GrpcRetrier::GrpcStatusCodes &statusCodes,
    ExecuteOperationMetadataEntriesMultiMap *executeOperationMetadataEntries)
{
    std::atomic_bool shouldCancel(false);
    return updateBotSession(botStatus, stub, botsServerConnection, session,
                            statusCodes, shouldCancel,
                            executeOperationMetadataEntries);
}

grpc::Status BotSessionUtils::updateBotSession(
    const proto::BotStatus botStatus,
    std::shared_ptr<proto::Bots::StubInterface> stub,
    const buildboxcommon::ConnectionOptions &botsServerConnection,
    proto::BotSession *session,
    const GrpcRetrier::GrpcStatusCodes &statusCodes,
    const std::atomic_bool &cancelCall,
    ExecuteOperationMetadataEntriesMultiMap *executeOperationMetadataEntries)
{

    const auto updateInvocation = [&](grpc::ClientContext &context) {
        BUILDBOX_LOG_TRACE("Updating bot session (currently botstatus="
                           << BotStatus_Name(botStatus) << ")");
        grpc::CompletionQueue cq;
        proto::UpdateBotSessionRequest updateRequest;
        updateRequest.set_name(session->name());
        *updateRequest.mutable_bot_session() = *session;

        std::unique_ptr<
            grpc::ClientAsyncResponseReaderInterface<proto::BotSession>>
            reader_ptr =
                stub->AsyncUpdateBotSession(&context, updateRequest, &cq);
        grpc::Status status;
        reader_ptr->Finish(session, &status, nullptr);
        waitForResponseOrCancel(&context, &cq, &status, cancelCall);
        return status;
    };

    grpc::Status status;
    session->set_status(botStatus);

    { // Timed Block
        buildboxcommon::buildboxcommonmetrics::MetricGuard<
            buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
            mt(MetricNames::TIMER_NAME_UPDATE_BOTSESSION);

        const int retryLimit = std::stoi(botsServerConnection.d_retryLimit);
        const int retryDelay = std::stoi(botsServerConnection.d_retryDelay);
        const std::chrono::seconds requestTimeout = std::chrono::seconds(
            std::stoi(botsServerConnection.d_requestTimeout));

        GrpcRetrier retrier(retryLimit, std::chrono::milliseconds(retryDelay),
                            updateInvocation, "UpdateBotSession()", {},
                            requestTimeout);

        retrier.addOkStatusCode(grpc::StatusCode::CANCELLED);
        retrier.issueRequest();
        return retrier.status();
    }
}

grpc::Status BotSessionUtils::createBotSession(
    std::shared_ptr<proto::Bots::StubInterface> stub,
    const buildboxcommon::ConnectionOptions &botsServerConnection,
    proto::BotSession *session,
    const buildboxcommon::GrpcRetrier::GrpcStatusCodes &statusCodes)
{
    std::atomic_bool shouldCancel(false);
    return createBotSession(stub, botsServerConnection, session, statusCodes,
                            shouldCancel);
}

grpc::Status BotSessionUtils::createBotSession(
    std::shared_ptr<proto::Bots::StubInterface> stub,
    const buildboxcommon::ConnectionOptions &botsServerConnection,
    proto::BotSession *session,
    const buildboxcommon::GrpcRetrier::GrpcStatusCodes &statusCodes,
    const std::atomic_bool &cancelCall)
{
    grpc::ClientContext context;
    proto::CreateBotSessionRequest createRequest;

    const auto updateInvocation = [&](grpc::ClientContext &context) {
        BUILDBOX_LOG_DEBUG("Setting parent");
        createRequest.set_parent(botsServerConnection.d_instanceName);
        *createRequest.mutable_bot_session() = *session;
        BUILDBOX_LOG_DEBUG("Setting session");

        grpc::CompletionQueue cq;
        std::unique_ptr<
            grpc::ClientAsyncResponseReaderInterface<proto::BotSession>>
            reader_ptr =
                stub->AsyncCreateBotSession(&context, createRequest, &cq);
        grpc::Status status;
        reader_ptr->Finish(session, &status, nullptr);
        waitForResponseOrCancel(&context, &cq, &status, cancelCall);
        return status;
    };

    grpc::Status status;
    { // Timed Block
        buildboxcommon::buildboxcommonmetrics::MetricGuard<
            buildboxcommon::buildboxcommonmetrics::DurationMetricTimer>
            mt(MetricNames::TIMER_NAME_CREATE_BOTSESSION);

        const int retryLimit = std::stoi(botsServerConnection.d_retryLimit);
        const int retryDelay = std::stoi(botsServerConnection.d_retryDelay);
        const std::chrono::seconds requestTimeout = std::chrono::seconds(
            std::stoi(botsServerConnection.d_requestTimeout));

        GrpcRetrier retrier(retryLimit, std::chrono::milliseconds(retryDelay),
                            updateInvocation, "CreateBotSession()", {},
                            requestTimeout);
        retrier.addOkStatusCode(grpc::StatusCode::CANCELLED);
        retrier.issueRequest();
        return retrier.status();
    }
}

BotSessionUtils::ExecuteOperationMetadataEntriesMultiMap
BotSessionUtils::extractExecuteOperationMetadata(
    const GrpcServerMetadataMultiMap &metadata)
{
    const auto executeOperationMetadataName = "executeoperationmetadata-bin";

    ExecuteOperationMetadataEntriesMultiMap res;
    for (const auto &entry : metadata) {
        if (entry.first == executeOperationMetadataName) {
            ExecuteOperationMetadata message;
            if (message.ParseFromString(
                    std::string(entry.second.data(), entry.second.length()))) {
                res.emplace(message.action_digest(), message);
            }
        }
    }
    return res;
}

void BotSessionUtils::waitForResponseOrCancel(
    grpc::ClientContext *context, grpc::CompletionQueue *cq,
    grpc::Status *status, const std::atomic_bool &cancelCall)
{
    void *tag;
    bool ok;
    while (true) {
        const auto deadline =
            std::chrono::system_clock::now() + std::chrono::milliseconds(250);
        const auto asyncStatus = cq->AsyncNext(&tag, &ok, deadline);
        if (asyncStatus != grpc::CompletionQueue::TIMEOUT) {
            // Either we got an event on the queue or the
            // completion queue was shutdown. Both mean
            // we break out of the loop
            break;
        }
        if (cancelCall) {
            context->TryCancel();
            cq->Shutdown();
        }
    }
}

} // namespace buildboxworker
