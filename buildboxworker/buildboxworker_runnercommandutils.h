/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef INCLUDED_RUNNER_COMMAND_UTILS
#define INCLUDED_RUNNER_COMMAND_UTILS

#include <string>
#include <vector>

#include <buildboxcommon_connectionoptions.h>

namespace buildboxworker {

struct RunnerCommandUtils {
    /*
     * Given the necessary arguments that a runner invocation takes, return a
     * vector of command-line arguments.
     */
    static std::vector<std::string> buildRunnerCommand(
        const std::string &runnerBinaryPath,
        const std::vector<std::string> &runnerExtraArguments,
        const std::string &actionFilePath,
        const std::string &actionResultFilePath,
        const buildboxcommon::ConnectionOptions &casConnectionOptions,
        const std::string &logLevel, const std::string &logDirectory);

    /*
     * Given a runner command and a path, set the options to redirect stdout
     * and stderr to the respective files.
     */
    static void setRunnerStderrStdoutFile(const std::string &stdoutFilePath,
                                          const std::string &stderrFilePath,
                                          std::vector<std::string> *command);
};

} // namespace buildboxworker

#endif
